#
# Copyright (C) 2018-2019 The Google Pixel3ROM Project
# Copyright (C) 2020 Raphielscape LLC. and Haruka LLC.
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

WITH_GMS := true

# product/app
PRODUCT_PACKAGES += \
    GoogleContactsSyncAdapter \
    GoogleTTS \
    LocationHistoryPrebuilt \
    MarkupGoogle \
    SoundPickerPrebuilt

# product/priv-app
PRODUCT_PACKAGES += \
    AndroidAutoStubPrebuilt \
    ConfigUpdater \
    GoogleOneTimeInitializer \
    GoogleRestorePrebuilt \
    Phonesky \
    SetupWizardPrebuilt \
    TurboPrebuilt \
    Velvet \
    WfcActivation

# system/app
PRODUCT_PACKAGES += \
    GoogleExtShared

# system_ext/app
PRODUCT_PACKAGES += \
    Flipendo

# system_ext/priv-app
PRODUCT_PACKAGES += \
    GoogleServicesFramework \
    TurboAdapter

# PrebuiltGmsCore
PRODUCT_PACKAGES += \
    PrebuiltGmsCoreSc

PRODUCT_PACKAGES += \
    libprotobuf-cpp-full \
    librsjni

$(call inherit-product, vendor/gapps/product/blobs/product_blobs.mk)
$(call inherit-product, vendor/gapps/system/blobs/system_blobs.mk)
$(call inherit-product, vendor/gapps/system_ext/blobs/system-ext_blobs.mk)
